--DDL (DATA DEFINITION LANGUAGE)-------------------------------------------------------------

/*CREAZIONE DATABASE*/
CREATE SCHEMA Nome --creazione del database chiamato NOme
AUTHORIZATION Utente --utente è l'amministratore
Definizioni

/*CREAZIONE RELAZIONE*/
CREATE TABLE S ( --definisce un nuovo schema di relazione chiamato S
id char(5), --crea l'attibuto id specificando il tipo di dato char
val1 varchar(30),
val2 date )

/*CREAZIONE DOMINIO SU ATTRIBUTO*/
CREATE DOMAIN val2 --definisce il dominio di val2 come smallint, possibilità di essere NULL e compreso tra 18 e 30
AS SMALLINT DEFAULT NULL
CHECK (value >= 18 AND value <= 30)

/*CANCELLAZIONE*/
DROP SCHEMA Nome --se con il comando Restrict, il drop non viene eseguito se il database non è vuoto
[RESTRICT | CASCADE] --; se con Cascade vengono rimossi automaticamente tutti i dati presenti
